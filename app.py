import base64
import codecs
import json
import io

from flask import Flask, request, jsonify
from flask_json import FlaskJSON, JsonError, as_json
from PIL import Image

import layoutlm.preprocessing.preprocess_image as preprocess
import layoutlm.examples.classification.run_classification as classification

app = Flask(__name__)

# Don't add an extra "status" property to JSON responses - this would break the API contract
app.config['JSON_ADD_STATUS'] = False
# Don't sort properties alphabetically in response JSON
app.config['JSON_SORT_KEYS'] = False

json_app = FlaskJSON(app)


@json_app.invalid_json_error
def invalid_request_error(e):
    """Generates a valid ELG "failure" response if the request cannot be parsed"""
    raise JsonError(status_=400, failure={'errors': [
        {'code': 'elg.request.invalid', 'text': e}
    ]})


@app.route('/', methods=['GET'])
def starting_url():
    return 'Success', 200


@app.route('/process/img2class', methods=['POST'])
def img2class():
    if request.content_type.split(";", 1)[0] != "multipart/form-data":
        err = "Error:Multipart/from-data expected, not " + request.content_type.split(";", 1)[0]
        invalid_request_error(err)

    # # Uncomment to process meta
    # if 'request' in request.files:
    #     r = request.files['request']
    #     print(r)
    #     if r.content_type != "application/json":
    #         err = "Error:Multipart metadata: application/json expected, not " + r.content_type
    #         invalid_request_error(err)
    #     print(request.files['request'])
    #     meta = json.loads(request.files['request'])

    if 'content' not in request.files:
        err = "Error:Multipart content missing"
        invalid_request_error(err)

    content = request.files['content']
    try:
        img = Image.open(io.BytesIO(content.read()))
    except:
        invalid_request_error("Error:Wrong file format")
    preprocess.preprocess(img)
    prediction = classification.main()
    return dict(response={'type': 'classification',
                          'classes': [{'class': prediction}]})


@app.route('/process/hocr2class', methods=['POST'])
def hocr2class():
    if request.content_type.split(";", 1)[0] != "application/json":
        err = "Error:Multipart metadata: application/json expected, not " + request.content_type.split(";", 1)[0]
        invalid_request_error(err)

    data = json.loads(request.json)
    if data["type"] != "structuredText":
        err = "Error:Data type: structuredText expected, not " + data["type"]
        invalid_request_error(err)

    preprocess.save_hocr(data["texts"][0]["content"])
    prediction = classification.main()
    return dict(response={'type': 'classification',
                          'classes': [{'class': prediction}]})


@app.route('/process/img2hocr', methods=['POST'])
def img2hocr():
    if request.content_type.split(";", 1)[0] != "multipart/form-data":
        err = "Error:Multipart/from-data expected, not " + request.content_type.split(";", 1)[0]
        invalid_request_error(err)

    # # Uncomment to process meta
    # if 'request' in request.files:
    #     r = request.files['request']
    #     print(r)
    #     if r.content_type != "application/json":
    #         err = "Error:Multipart metadata: application/json expected, not " + r.content_type
    #         invalid_request_error(err)
    #     print(request.files['request'])
    #     meta = json.loads(request.files['request'])

    if 'content' not in request.files:
        err = "Error:Multipart content missing"
        invalid_request_error(err)

    content = request.files['content']
    try:
        img = Image.open(io.BytesIO(content.read()))
    except:
        invalid_request_error("Error:Wrong file format")

    preprocess.preprocess(img)
    with open('data/hocr.xml', 'r', encoding='utf-8') as file:
        xml_str = file.read()

    return dict(response={'type': 'structuredText',
                          'texts': [{"content": xml_str,
                                     "mimeType": "text/plain",
                                     "features": {"fileType": "xml", "contentType": "hocr"}
                                     }]})


if __name__ == "__main__":
    app.run(debug=True)
