#!/usr/bin/env sh

exec /sbin/tini -- gunicorn --bind=0.0.0.0:8000 "--workers=$WORKERS" "--timeout=$TIMEOUT" --worker-tmp-dir=/dev/shm "$@" app:app