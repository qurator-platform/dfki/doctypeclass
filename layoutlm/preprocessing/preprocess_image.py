import os
from pathlib import Path

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

# If you don't have tesseract executable in your PATH, include the following:
# pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'


def preprocess(img):
    data = pytesseract.image_to_pdf_or_hocr(img, extension='hocr')

    with open(os.path.join(Path(__file__).parent.parent.parent, "data", "hocr.xml"), "w", encoding='utf-8') as writer:
        writer.write(data.decode('utf-8'))


def save_hocr(data):
    with open(os.path.join(Path(__file__).parent.parent.parent, "data", "hocr.xml"), "w", encoding='utf-8') as writer:
        writer.write(data)
