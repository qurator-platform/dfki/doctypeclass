# coding=utf-8
from __future__ import absolute_import, division, print_function

import argparse
import logging
import os
import random
from pathlib import Path

import numpy as np
import torch
from torch.utils.data import DataLoader, SequentialSampler
from tqdm import tqdm
from transformers import (
    BertTokenizerFast,
)

from layoutlm.layoutlm import LayoutlmConfig, LayoutlmForSequenceClassification
from layoutlm.layoutlm.data.rvl_cdip import CdipProcessor, load_and_cache_example

try:
    from torch.utils.tensorboard import SummaryWriter
except:
    from tensorboardX import SummaryWriter


logger = logging.getLogger(__name__)

ALL_MODELS = sum(
    (
        tuple(LayoutlmConfig.pretrained_config_archive_map.keys())
    ),
    (),
)

MODEL_CLASSES = {
    "layoutlm": (LayoutlmConfig, LayoutlmForSequenceClassification, BertTokenizerFast),
}

LABELS = [
    "letter",
    "form",
    "email",
    "handwritten",
    "advertisement",
    "scientific report",
    "scientific publication",
    "specification",
    "file folder",
    "news article",
    "budget",
    "invoice",
    "presentation",
    "questionnaire",
    "resume",
    "memo"
]


def set_seed(args):
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)


def simple_accuracy(preds, labels):
    return (preds == labels).mean()


def predict(args, model, tokenizer, prefix=""):
    eval_dataset = load_and_cache_example(args, tokenizer)

    if not os.path.exists(args.output_dir) and args.local_rank in [-1, 0]:
        os.makedirs(args.output_dir)

    eval_sampler = SequentialSampler(eval_dataset)
    eval_dataloader = DataLoader(
        eval_dataset, sampler=eval_sampler
    )

    logger.info("***** Running evaluation {} *****".format(prefix))
    logger.info("  Num examples = %d", len(eval_dataset))
    preds = None
    for batch in tqdm(eval_dataloader, desc="Predicting"):
        model.eval()
        if args.model_type != "layoutlm":
            batch = batch[:4]
        batch = tuple(t.to(args.device) for t in batch)

        with torch.no_grad():
            inputs = {
                "input_ids": batch[0],
                "attention_mask": batch[1],
                "bbox": batch[3],
                "token_type_ids": batch[2]
            }
            outputs = model(**inputs)
            logits = outputs[0]

        preds = logits.detach().cpu().numpy()

    preds = np.argmax(preds, axis=1)
    return LABELS[preds[0]]


def main():
    parser = argparse.ArgumentParser()

    ## Required parameters
    parser.add_argument(
        "--data_dir",
        default=os.path.join(Path(__file__).parent.parent.parent.parent, "data"),
        type=str,
        help="The input data dir. Should contain the .tsv files (or other data files) for the task.",
    )
    parser.add_argument(
        "--model_type",
        default="layoutlm",
        type=str,
        help="Model type selected in the list: " + ", ".join(MODEL_CLASSES.keys()),
    )
    parser.add_argument(
        "--model_name_or_path",
        default=os.path.join(Path(__file__).parent.parent.parent.parent, "model"),
        type=str,
        help="Path to pre-trained model or shortcut name selected in the list: "
        + ", ".join(ALL_MODELS),
    )
    parser.add_argument(
        "--output_dir",
        default=os.path.join(Path(__file__).parent.parent.parent.parent, "model"),
        type=str,
        help="The output directory where the model predictions and checkpoints will be written.",
    )

    # Other parameters
    parser.add_argument(
        "--max_seq_length",
        default=512,
        type=int,
        help="The maximum total input sequence length after tokenization. Sequences longer "
        "than this will be truncated, sequences shorter will be padded.",
    )

    parser.add_argument(
        "--do_test",
        default=True,
        action="store_true",
        help="Whether to run test on the test set."
    )

    parser.add_argument(
        "--do_lower_case",
        default=True,
        action="store_true",
        help="Set this flag if you are using an uncased model.",
    )

    parser.add_argument(
        "--logging_steps", type=int, default=50, help="Log every X updates steps."
    )
    parser.add_argument(
        "--save_steps",
        type=int,
        default=50,
        help="Save checkpoint every X updates steps.",
    )
    parser.add_argument(
        "--no_cuda",
        default=True,
        action="store_true",
        help="Avoid using CUDA when available"
    )
    parser.add_argument(
        "--overwrite_output_dir",
        default=True,
        action="store_true",
        help="Overwrite the content of the output directory",
    )
    parser.add_argument(
        "--seed", type=int, default=42, help="random seed for initialization"
    )

    args, unknown = parser.parse_known_args()
    args.device = torch.device("cpu")

    # Setup logging
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.WARN,
    )
    logger.warning(
        "Device: %s",
        args.device
    )

    # Set seed
    set_seed(args)

    processor = CdipProcessor()
    label_list = processor.get_labels()
    num_labels = len(label_list)

    # Load pretrained model and tokenizer
    args.model_type = args.model_type.lower()
    config_class, model_class, tokenizer_class = MODEL_CLASSES[args.model_type]
    config = config_class.from_pretrained(
        args.model_name_or_path,
        num_labels=num_labels,
    )
    model = model_class.from_pretrained(
        args.model_name_or_path,
        from_tf=bool(".ckpt" in args.model_name_or_path),
        config=config,
    )

    model.to(args.device)

    logger.info("Training/evaluation parameters %s", args)

    tokenizer = tokenizer_class.from_pretrained(
        args.output_dir, do_lower_case=args.do_lower_case
    )
    checkpoints = [args.output_dir]
    logger.info("Evaluate the following checkpoints: %s", checkpoints)
    for checkpoint in checkpoints:
        prefix = (
            checkpoint.split("/")[-1]
            if checkpoint.find("checkpoint") != -1 and args.eval_all_checkpoints
            else ""
        )

        model = model_class.from_pretrained(checkpoint)
        model.to(args.device)
        result = predict(args, model, tokenizer, prefix=prefix)

    return result


if __name__ == "__main__":
    main()
