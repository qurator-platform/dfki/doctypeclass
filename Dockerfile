FROM python:3-slim

RUN apt-get update && \
apt-get install --no-install-recommends -y software-properties-common && \
add-apt-repository -y ppa:alex-p/tesseract-ocr && \
apt-get install --no-install-recommends -y tesseract-ocr-all \
curl \
build-essential libssl-dev libffi-dev

# Install tini and create an unprivileged user
ADD https://github.com/krallin/tini/releases/download/v0.19.0/tini /sbin/tini
RUN addgroup --gid 1001 "elg" && \
      adduser --disabled-password --gecos "ELG User,,," \
      --home /elg --ingroup elg --uid 1001 elg && \
      chmod +x /sbin/tini

ENV RUSTUP_HOME=/rust
ENV CARGO_HOME=/cargo
ENV PATH="/cargo/bin:${PATH}"

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain none -y && \
rustup toolchain install nightly --allow-downgrade --profile minimal --component clippy && \
pip install --upgrade pip && \
pip install torch==1.7.1+cpu torchvision==0.8.2+cpu -f https://download.pytorch.org/whl/torch_stable.html \
pytesseract \
transformers==2.11 \
flask \
lxml \
tensorboardx \
flask_json \
gunicorn

COPY --chown=elg:elg . /elg/

# Everything from here down runs as the unprivileged user account
USER elg:elg

WORKDIR /elg

ENV WORKERS=1
ENV TIMEOUT=0

EXPOSE 5000

ENTRYPOINT ["sh", "/elg/entrypoint.sh"]
#CMD ['/sbin/tini', '--', 'venv/bin/gunicorn', '--bind=0.0.0.0:8000', "--workers=$WORKERS", '--worker-tmp-dir=/dev/shm "$@" app:app']
