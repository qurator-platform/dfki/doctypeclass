import requests
import json
import time

# change to your localhost
url = "http://192.168.99.100:5000/process"\

session = requests.Session()

def post_img(address):
    meta = {
        'type': 'image',
        'format': '.png',
    }

    multipart_form_data = {
        'content': ('img.png', open('largepreview.png', 'rb')),
        'request': (None, json.dumps(meta), 'application/json'),
    }

    r = session.post(url + address, files=multipart_form_data)
    return r.content


def post_hocr():
    with open('hocr_test.xml', 'r', encoding='utf-8') as file:
        xml_str = file.read()

    structured_text = {
        "type": "structuredText",
        "texts": [
            {
                "content": xml_str,
                "mimeType": "text/plain",
                "features": {"fileType": "xml", "contentType": "hocr"}
            }
        ]
    }

    r = session.post(url + '/hocr2class', json=json.dumps(structured_text))
    return r.content


# With the running docker container from registry.gitlab.com/qurator-platform/dfki/doctypeclass
start_time = time.time()
# Image classification request
print(post_img('/img2class'))
# HOCR (preprocessed image) classification request
print(post_hocr())
# Image preprocessing (to HOCR) request
# print(post_img('/img2hocr'))
print("--- %s seconds ---" % (time.time() - start_time))